package com.example.demo.dto;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;


import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="Salas")
public class Salas {

	//Atributos de entidad cliente
		@Id
		@GeneratedValue(strategy = GenerationType.IDENTITY)
		@Column(name = "codigo")
		private int codigo;
		@Column(name = "nombre")
		private String nombre;

	    @ManyToOne
	    @JoinColumn(name="pelicula")
	    private Peliculas peliculas;

	    
	    
		public Salas(int codigo, String nombre, Peliculas peliculas) {
			super();
			this.codigo = codigo;
			this.nombre = nombre;
			this.peliculas = peliculas;
		}
		
		public Salas() {

		}

		public int getCodigo() {
			return codigo;
		}

		public void setCodigo(int codigo) {
			this.codigo = codigo;
		}

		public String getNombre() {
			return nombre;
		}

		public void setNombre(String nombre) {
			this.nombre = nombre;
		}
		
		public Peliculas getPeliculas() {
			return peliculas;
		}

		public void setPeliculas(Peliculas peliculas) {
			this.peliculas = peliculas;
		}

		@Override
		public String toString() {
			return "Salas [codigo=" + codigo + ", nombre=" + nombre + ", peliculas=" + peliculas + "]";
		}

		

		

		


	    


	    
		
		
	    
		
		
		
		

		
	
}