package com.example.demo.dto;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;


import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="departamentos")
public class Departamentos {

	//Atributos de entidad cliente
		@Id
		@GeneratedValue(strategy = GenerationType.IDENTITY)
		@Column(name = "codigo")
		private int codigo;
		@Column(name = "nombre")
		private String nombre;
		@Column(name = "presupuesto")
		private int presupuesto;

		
	    @OneToMany
	    @JoinColumn(name="dni")
	    private List<Empleados> empleado;
	    
	  //Constructores
	    
	    public Departamentos() {
	    	
		}
	    
	    public Departamentos(int codigo, String nombre, int presupuesto) {
			this.codigo = codigo;
			this.nombre = nombre;
			this.presupuesto = presupuesto;

		}
	    
	  //Getters y Setters
	    
	    public int getCodigo() {
			return codigo;
		}

	
		public void setCodigo(int id) {
			this.codigo = id;
		}

	
		public String getNombre() {
			return nombre;
		}

	
		public void setNombre(String nombre) {
			this.nombre = nombre;
		}

	
		public int getpresupuesto() {
			return presupuesto;
		}

		
		public void setpresupuesto(int presupuesto) {
			this.presupuesto = presupuesto;
		}
		
		/**
		 * @return the video
		 */
		@JsonIgnore
		@OneToMany(fetch = FetchType.LAZY, mappedBy = "Empleados")
		public List<Empleados> getEmpleados() {
			return empleado;
		}

		/**
		 * @param video the video to set
		 */
		public void setEmpleados(List<Empleados> Empleados) {
			this.empleado = empleado;
		}

		@Override
		public String toString() {
			return "Departamentos [codigo=" + codigo + ", nombre=" + nombre + ", presupuesto=" + presupuesto
					+ ", empleado=" + empleado + "]";
		}

		
		
		

		
	
}